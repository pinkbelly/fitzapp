class Task < ActiveRecord::Base
  belongs_to :user

  validates_presence_of :text, :user

  def is_old?
    past = 2.weeks.ago
    past > created_at
  end
end
