class User < ActiveRecord::Base
  has_secure_password

  has_many :tasks

  validates_presence_of :name, :password
  validates_uniqueness_of :name
  validates_length_of :name, :within => 2..20, :too_long => "must be less than 20 characters.", :too_short => "must be at least 2 characters."
  validates_length_of :password, :within => 6..15, :too_long => "must be less than 15 characters.", :too_short => "must be at least 6 characters."
end
