Fitzapp::Application.routes.draw do
  resources :tasks
  resources :users, only: [:new, :create, :edit, :update, :show]
  resources :sessions, only: [:new, :create, :destroy]
  root 'tasks#index'
end
