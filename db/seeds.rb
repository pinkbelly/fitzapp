User.destroy_all
Task.destroy_all

module Seeder
  class FakeUser
    attr_reader :id

    def initialize(name, password)
      @name = name
      @password = password
    end

    def create
      params = ActionController::Parameters.new({
        name: @name,
        password: @password,
        password_confirmation: @password
        })
      user = User.create!(params.permit(:name, :password, :password_confirmation))
      @id = user.id
    end
  end

  class FakeTask
    def initialize(user, completed=false)
      @user_id = user.id
      @completed = completed
    end

    def create
      params = ActionController::Parameters.new({
        text: task_text,
        completed: @completed,
        user_id: @user_id
      })
      Task.create!(params.permit(:text, :completed, :user_id))
    end

    def task_text
      "#{Faker::Commerce.color.capitalize} #{Faker::Commerce.color} #{Faker::Commerce.color} #{Faker::Commerce.color}"
    end
  end
end

colin = Seeder::FakeUser.new('colin', 'kiwibird')
liz = Seeder::FakeUser.new('liz', 'tacoparty')

colin.create
liz.create

10.times do
  task = Seeder::FakeTask.new(colin)
  task.create
end

3.times do
  task = Seeder::FakeTask.new(colin, true)
  task.create
end
