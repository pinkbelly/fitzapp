FactoryGirl.define do
  factory :user do
    name                    'user'
    password                'password'
    password_confirmation   'password'
  end

  factory :task do
    text 'Buy treats for dog'
    user

    trait :complete do
      completed true
    end

    trait :incomplete do
      completed false
    end
  end
end