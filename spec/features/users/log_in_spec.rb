require_relative '../../spec_helper'

feature 'Logging in' do
  background do
    @user = create(:user)
    visit '/sessions/new'
  end

  scenario 'with the correct credentials' do
    within('#new_session') do
      fill_in 'Name', with: 'user'
      fill_in 'Password', with: 'password'
    end
    click_button 'Sign in'
    expect(page).to have_content('Welcome user')
  end

  scenario 'with the wrong name' do
  end

  scenario 'with the wrong password' do
  end
end