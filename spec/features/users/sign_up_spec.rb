require_relative '../../spec_helper'

# group of tests for the sign up feature
feature 'Signing up' do
  # run before *each* test inside of this feature block
  background do
    visit '/users/new'
  end

  # single test for a proper sign in
  scenario 'with the correct credentials' do
    # #new_user is the id of the form
    # everything inside of this block is done within the form
    within('#new_user') do
      # user_name is the id of the form field
      # use the id to prevent problems with ambiguous matches
      # you can use fill_in 'Name' if you want
      fill_in 'user_name', with: 'fitzpants'
      # ambiguous match example!
      # fill_in 'Password' would return an ambiguous match
      # because one field is 'Password', the other is
      # 'Password confirmation'
      # choose ids or text and stick with it
      fill_in 'user_password', with: 'tacoparty'
      fill_in 'user_password_confirmation', with: 'tacoparty'
    end
    click_button 'Submit'
    expect(page).to have_content 'User was successfully created.'
  end

  scenario 'with an invalid name' do
    # todo
  end

  scenario 'with an invalid password' do
    # todo
  end
end