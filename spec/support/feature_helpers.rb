module FeatureHelpers
  def log_in
    @user = create(:user)
    visit '/sessions/new'
    within('#new_session') do
      fill_in 'Name', with: 'user'
      fill_in 'Password', with: 'password'
    end
    click_button 'Sign in'
  end
end